// Odroid HC1 end cover
$fn=60;

module odroidHCx(){
    translate([0,0,-1]) cube([56,46,1]); //board
    //cube([56,46,3]); //parts
    
    translate([37.8,-1.3,0]) 
    union(){
        cube([16.0,21.6,13.6]); //Net
        translate([2,-10,3.5]) cube([12.5,10,9.5]); //Net conn
    }
    
    translate([28.6,-1.5,-0.5]) 
    union(){
        cube([7.3,19.5,14.7]); //Usb
        translate([1,-12.5,0.5]) cube([5.3,12.5,13.7]); //Usb conn     
    }

    translate([19,-1.5,0]) 
    union(){
        cube([7.5,11.6,10]); //Pwr 
        //translate([31.5+0.5,-12.5,-0.5+3.5]) cube([6.5,12.5,6.5]); //Pwr conn
        translate([7.5/2,0,6.3]) rotate([90,0,0])cylinder(d=6,h=15); //Pwr conn
        translate([7.5/2,-2,6.3]) rotate([90,0,0])cylinder(d=9,h=15); //Pwr conn
    }
    
    translate([5.6,0,0]) 
    union(){
        cube([11,10,3]); //uSD
        translate([0,-7,1]) cube([11,7.5,2]); //uSD
    }
}


module HC2(){
    
    translate([11.5,-0.8,-0.5]) 
    minkowski(){
    odroidHCx();
    sphere(0.4);
        }
    
    translate([0,0,-7]) cube([79,10,7]); // lower profile

    //translate([-4.35,0,-5]) cube([111,10,23]);

}

module cooling(){
    translate([0,0,0.8])
    difference(){
        union(){
            translate([0-0.25,0,0])cube([78.6+0.5,100,12.5]);
            translate([(78.6-80.5)/2-0.25,0,7])cube([80.5,100,1.1]);
        }
        translate([(78.6-71)/2,0,0])cube([71,100,12.5]); // center
        translate([(78.6-74.5)/2,0,12.5-2])cube([74.5,100,2]); //upper center
        
        translate([-0.5,0,7+1.1])cube([1.1,100,1.1]); // left groove
        translate([78.6-1.1+0.5,0,7+1.1])cube([1.1,100,1.1]); // right groove
    }
}


module cover(){
    hull(){
    translate([-1,-2,0]) cube([79+2,2,12.5+0.8+2.0]); // front covfefe
    
    translate([10+3.5,-4.5,-5]) cube([52,4.5,20-1]); // reenforcement covfefe
    }
    translate([-1-1,-1,8.9]) cube([3,47,5]); // left covfefe
    translate([79-1,-1,8.9]) cube([3,47,5]); // right covfefe
    
    wup = 79+2+2;
    translate([-1-1,-1,12.5+0.8]) cube([wup,47+1+2,2.0]); // upper covfefe
    
    translate([10.5+4.25,-4,1.5]) rotate([0,90,0]) cylinder(d=8,h=15.5,$fn=16); //uSD
    
    difference(){ // radius
        rad= 4;
        translate([0,0,14-rad]) cube([79,rad,rad]); 
        translate([0,rad,14-rad]) rotate([0,90,0])  cylinder(r=rad,h=103);
    }
    translate([4,45+2,10-2])cube([71,2,5.5]);
    translate([0-0.25,45+2,0])
    difference(){ // radius
        rad= 4;
        translate([4,-rad,14-rad]) cube([79-8,rad,rad]); 
        translate([0,-rad,14-rad]) rotate([0,90,0])  cylinder(r=rad,h=103);
    }
}


//rotate([0,180,0])

difference(){
cover();
color("red") HC2();
color("red") cooling();
}



