$fn =60;

use <libCopterCams.scad>
use <libCopterTXs.scad>

campos=[0,-3,9.5];

camwidth = 14.4;
wall = 2.5;
squaremeasure = 16;
camangle = 25;

module copy_mirror(vec=[0,1,0])
{
    children();
    mirror(vec) children();
}

module squarepattern(diff){
    translate([diff/2,diff/2,0]) children();
    translate([-diff/2,diff/2,0]) children();
    translate([diff/2,-diff/2,0]) children();
    translate([-diff/2,-diff/2,0]) children();
}


module camholder(squaremeasure, camangle = camangle, vis = true){
difference()
{
    union()
    {
        squarepattern(squaremeasure){cylinder(d=3.7,h=2);}
        translate([0,0,0])hull() squarepattern(squaremeasure){cylinder(d=3.7,h=1.0); // base plate
            }
    
        copy_mirror([1,0,0])
        {
        translate([camwidth/2,0,0])
        hull()
        {
        translate([0,-squaremeasure/2+3.7/2,0]) cube([(squaremeasure+3.7-camwidth)/2,squaremeasure-3.7,2]);
        translate(campos) rotate([0,90,0]) cylinder(d=6,h=wall); // upper eyes
        }
        translate([camwidth/2,0,0])
        translate([0,-squaremeasure/2,0]) cube([(squaremeasure+3.7-camwidth)/2,squaremeasure,2]);
        }
    
    }
    translate(campos) rotate([0,90,0]) cylinder(d=2,h=30,center=true); //cam drill 
    squarepattern(squaremeasure) // screw clearance
    {
        cylinder(d=1.6,h=8);
        translate([0,0,2]) cylinder(d1=4,d2=1,h=8);
    }
    cylinder(d1=squaremeasure,d2=squaremeasure/2,h=10);
}

    if(vis)
    {
        translate(campos) rotate([90-camangle,0,0])RUNCAM_NANO4(false);
        translate([0,0,30]) rotate ([0,0,180]) TX03i();
    }
}

camholder(squaremeasure);