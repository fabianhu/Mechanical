/*
Copter parts TX library

(c) 2018 Fabian Huslik

https://github.com/fabianhu/
*/ 
translate([-30,0,0]) rotate ([40,0,0]) TX03i();
translate([0,0,0]) rotate ([40,0,0]) TX03();
translate([50,0,0]) TX_MM213TL();

translate([-30,40,0]) rotate ([40,0,0]) TX03i(true);
translate([0,40,0]) rotate ([40,0,0]) TX03(true);
translate([50,40,0]) TX_MM213TL(true);

module TX03(clr=false)
{
  color("green") cube([19.6,17,3.5],center=true); // base PCA
  color("grey") translate([1,11.3,1.5]) cube([6.5,6,5],center=true);  // antenna PCA
  color("grey") translate([-6,2.5,2]) cube([7.6,10,3],center=true);  // LED
  color("grey") translate([-6.3,7,-1]) cube([5,4,2],center=true);  // SW
  color("grey") translate([1,14,2]) rotate([-90,0,0]) cylinder(d=2.5,h=54,$fn=12);
  color("grey") translate([1,14+30,2]) rotate([-90,0,0]) cylinder(d=5,h=10);
  color("grey") translate([6,-7,1.5]) cube([7,4,3],center=true);  // cabling
  if(clr)
  {
       color("red") translate([-6.5,9,-1]) rotate([-90,0,0]) cylinder(d=3,h=30); // actuation of switch
       color("red") translate([6,-7,1.5]) cube([7,4,10],center=true);  // cabling
       //color("red") translate([0,0,-1.5]) cube([19.6+1,17+1,4+1+3],center=true); // base PCA
       color("red") translate([-6,2.5,2+1]) cube([7.6+0.3,10+0.3,5],center=true);  // LED
  }
}

module TX03i(clr=false)
{
  color("green") cube([19.6,17,3.5],center=true); // base PCA
  color("grey") translate([1,11.3,1.5]) cube([6.5,6,3.5],center=true);  // antenna PCA
  color("grey") translate([-6,2.5,2]) cube([7.6,10,3],center=true);  // LED
  color("grey") translate([-6.3,7,-1]) cube([5,4,2],center=true);  // SW
  color("grey") translate([1,14,2]) rotate([90,0,0]) cylinder(d=2.5,h=54,$fn=12);
  color("grey") translate([6,-7,1.5]) cube([7,4,3],center=true);  // cabling
  if(clr)
  {
       color("red") translate([-6.5,9,-1]) rotate([-90,0,0]) cylinder(d=3,h=30); // actuation of switch
       color("red") translate([6,-7,1.5]) cube([7,4,6],center=true);  // cabling
       //color("red") translate([0,0,-1.5]) cube([19.6+1,17+1,4+1+3],center=true); // base PCA
       color("red") translate([-6,2.5,2+1]) cube([7.6+0.3,10+0.3,5],center=true);  // LED
  }
}


module TX_MM213TL(clr=false)
{
	color("grey") cube([19.5,22.5,3.5],true);

	color("grey") translate([5,-10.5,0]) rotate ([0,90,0])  cylinder(d=4,h=27);
	color("red") if(clr)
        {
		translate([0,10,0.5]) cube([13,4,4],true);
	}
}


