/*
Indoor Copter
1103 motors

(c) 2018 Fabian Huslik

https://github.com/fabianhu/
*/ 

use <libCopterFCs.scad>
use <libCopterESCs.scad>
use <libCopterCams.scad>
use <libCopterTXs.scad>
use <libCopterMotors.scad>
use <Coptertop.scad>

$fn = 60;
radius = 4 * (25.4/2); // propeller radius
echo("Prop radius: ",radius);
thick = 1.2; // rest body

th=4;

dx=radius*2+8;
dy=radius*2+8;




angF = 45;
angR = 45;

gpspos=[0,24,-thick];
gpssize=[19,3.6,19];
FCpos = [0,-5,1.0];
BattSize=[13,42+8,17.5];
BattPos = [0,-25,thick+BattSize.z/2-th]; // BattSize z !

// helper to morror while keeping the original.
module copy_mirror(vec=[0,1,0])
{
    children();
    mirror(vec) children();
}

module squarepattern(diff){
    translate([diff/2,diff/2,0]) children();
    translate([-diff/2,diff/2,0]) children();
    translate([diff/2,-diff/2,0]) children();
    translate([-diff/2,-diff/2,0]) children();
}

module esc16(){
    cube([29.5,25,3.5],center=true);
}


module txcam(){
    translate([0,0,-9/2])cube([15,12.5,9],center= true);
    cylinder(d=10.2,h=16.5-9);
    translate([-2,-17/2-2.25,-9]) cube([7,17,9]);
}


module STUFF(exp=false)
{
    translate(gpspos) rotate([-90,0,0]) 
    union()
    {
        cube(gpssize,center=true);
        translate([0,5,0]) rotate([90,0,0])cylinder(d=gpssize.x-6,h=10);
        translate([0,gpssize.y/2,-gpssize.x/2]) rotate([90,0,0])cylinder(d=6,h=20);
    }

    translate([0,0,7+50]) translate(FCpos) rotate([0,-0,0]) REVO16x16(exp);
    translate([0,0,3+1.0+50]) translate(FCpos) rotate([0,-0,0]) esc16();
    translate([0,0,0+50]) rotate([0,0,0]) RX_XM(exp);

    copy_mirror([1,0,0]) translate([dx/2,dy/2,1.5+1]) rotate([0,0,180+angF]) motor1204(exp,th+1.5+1,dia=4);
    copy_mirror([1,0,0]) translate([dx/2,-dy/2,1.5+1]) rotate([0,0,180-angR]) motor1204(exp,th+1.5+1,dia=4);
    //translate(0,0,0) translate([0,0,-th]) squarepattern(16) cylinder(d=1.7,h=10);

    translate([0,0,-th/2]) rotate([0,0,45]) cube([166,th,th+0.1],true);
    translate([0,0,-th/2]) rotate([0,0,-45]) cube([166,th,th+0.1],true);
    
    translate([dx/2-2.5,dy/2,1]) rotate([90,0,0]) translate([0,0,-3])cylinder(d=2,h=dy+6);
    translate([-dx/2+2,dy/2,1]) rotate([90,0,0]) translate([0,0,-3])cylinder(d=2,h=dy+6);
    
    

}






module arm(){
    difference(){
        translate([0,0,-th])hull()
        {
            squarepattern(6) cylinder(d=5,h=th+1.5+1);
        }
        
    translate([-th/2,5,-th]) rotate([90,0,0]) cube([th,th,30]); // arm
    
    }
}


module body()
{
    copy_mirror([1,0,0]) translate([dx/2,dy/2,0]) rotate([0,0,-90+angF]) arm();
    copy_mirror([1,0,0]) translate([dx/2,-dy/2,0]) rotate([0,0,-90-angR]) arm();
    
    difference()
    {
        union(){
            
            hull(){    
            translate([FCpos.x,FCpos.y+9,-th/2]) cube([10,1,th],center=true);
            
            translate([gpspos.x,gpspos.y-thick,-th/2]) 
                rotate([-90,0,0]) 
                translate([0,0,0])cube([gpssize.x+thick*2,th,gpssize.z+thick*2],center=true);
            }
            
         }
         
         translate(gpspos) rotate([-90,0,0]) cube(gpssize,center=true);
        
    }
    
   
    //translate(BattPos)BattHld();
    
    translate(FCpos) fcholder();
    
    

}

module fcholder(){
    translate([0,0,-th-1])
    difference(){
        union(){  
            translate([0,0+5,-th/2+0.5+4]) cube([16+3,16+6+3,th+1],center = true); // center piece
            hull(){
                translate([8,8,0])cylinder(d=4.5,h=th+1);
                translate([-8,-8,0])cylinder(d=4.5,h=th+1);
            }
            hull(){
                translate([-8,8,0])cylinder(d=4.5,h=th+1);
                translate([8,-8,0])cylinder(d=4.5,h=th+1);
            }
            translate([0,0,th+1])squarepattern(16) cylinder(d1=4.5,d2=3.5,h=2.5);
        }
        squarepattern(16) cylinder(d=1.7,h=8); // bolt
        squarepattern(16) translate([0,0,th+3])cylinder(d=2.1,h=th); // bolt
      }
}


difference()
{
body();
STUFF(true);   

}  

translate([0,10,10]){

}

%STUFF(false);   

translate([0,-5,12])
camholder(16,20,false);
