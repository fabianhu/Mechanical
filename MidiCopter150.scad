/*
Medium sized (sub 250g) copter

(c) 2017-2018 Fabian Huslik

https://gitlab.com/fabianhu/
*/ 

use <libCopterFCs.scad>
use <libCopterESCs.scad>
use <libCopterCams.scad>
use <libCopterTXs.scad>
use <libCopterMotors.scad>

$fn=24;

StandOffDia = 4.0;

campos = [0,11,27];

lower_angle = 30;
upper_angle = 30;

lowerstand=3.5;
upperstand=4;

CoverThickness = 1.8;

isDemo=false;

isIntegratedAntenna = false;


module copy_mirror(vec=[0,1,0])
{
    children();
    mirror(vec) children();
}

module copy_rotate(vec=[0,90,0])
{
    children();
    rotate(vec) children();
}

module Standoffs()
{
    translate([-20,30,0])
    for(i=[0:10:40])
    {
        difference()
        {
            translate([i,0,0]) cylinder(d=StandOffDia,h=lowerstand);
            translate([i,0,0]) cylinder(d=2.1,h=lowerstand+0.5);
        }
        difference()
        {
            translate([i,10,0]) cylinder(d=StandOffDia,h=upperstand);
            translate([i,10,0]) cylinder(d=2.1,h=upperstand+0.5);
        }

        difference()
        {
            translate([i,20,0]) cylinder(d=7,h=2.5);
            translate([i,20,0]) cylinder(d=2.0,h=2.5+0.5);
        }
    }
}
Standoffs();

module BODY() 
{
	lb= 23; // top piece length
	wb= 23; // top piece width
	h=7; // top piece height offset
	r= 3.5; // top piece radius
    ht= 5; // top piece thickness

	difference()
    {
        union()
        {
            hull() // inner top piece
            {
                w= (wb-r)/2;
                d = (lb-r)/2;
                for(i = [ 
                         [  w,   d,  8.5],
                         [ w, -d, 8.5],
                         [-w,  d, 8.5],
                         [-w,  -d, 8.5] ])
                {
                    translate (i) cylinder(h=ht,d=StandOffDia+1, $fn=30);
                }  
                translate(campos) rotate([0,90,0]) cylinder(d=6,h=24,center=true);
            }
            translate([0,0,12.5]) cube([28,20,1],center=true); // RX support, max w=36
            
        } // end body		
		
		STUFF(true); // remove all inner stuff
		//color("red") COVER(); // for occasionally checking - slows down everything
        
		// bolt holes (thread)
		for(i = [ 
		         [  10,   10,  0],
		         [ 10, -10, 0],
		         [-10,  10, 0],
		         [-10,  -10, 0] ])
		{
			translate (i) cylinder(h=9+7,d=1.75, $fn=30);
		}
        
        // bolt holes (free)
		for(i = [ 
		         [  10,   10,  0],
		         [ 10, -10, 0],
		         [-10,  10, 0],
		         [-10,  -10, 0] ])
		{
			translate (i) cylinder(9.5,d=2.25, $fn=30);
		}
 
    }
}

module extensionbox()
{
    minkowski()
    {
        union()
        {
            cube([19,16,19],center=true);
            translate([0,-10,0]) cube([27,5,15],center=true);
        }
        cube(2,center=true);
    } // cam extension
}

InnerCoverPillarDia = 14;

module innerCover(rem=false)
{
    h=16;
    
    hull() 
    {
        translate(campos) rotate([0,90,0]) cylinder(d=6,h=10+10+7,center=true); // cam support cylinder
        translate(campos)  rotate([-90+(lower_angle+upper_angle)/2,0,0]) translate([0,0,7.5+0.6]) cylinder(d=16,h=4,center=true); // Protector ring

        translate(campos)  rotate([(lower_angle),0,0]) translate([0,-10+3,+0.5]) extensionbox();
        
        translate(campos) translate([0,0,3]) sphere(d=20);

        copy_mirror([0,1,0]) copy_mirror([1,0,0]) translate ([11, 10, 0]) cylinder(h=h,d=rem?InnerCoverPillarDia:InnerCoverPillarDia+3, $fn=24);

        if(isIntegratedAntenna)
        {
            Antenna();
        }
        STUFF(false);

    }
    if(rem)
    {
        rotate([0,0,45]) copy_rotate([0,0,90]) copy_mirror([0,1,0]) translate([0,52.6/2+2.4,0]) cylinder(h=6,d=2.2, $fn=12);  // bottom bolt hole
        rotate([0,0,45]) copy_rotate([0,0,90]) copy_mirror([0,1,0]) translate([0,52.6/2+2.4,3.0]) cylinder(h=6,d=4.75, $fn=6);  // bottom nut hexagon
        //rotate([0,0,45]) copy_rotate([0,0,90]) copy_mirror([0,1,0]) translate([0,52.6/2+2.4,4.8]) cylinder(h=7,d1=5.5,d2=9, $fn=30); // bottom nut clearance
        
       translate(campos)  rotate([-90+(lower_angle+upper_angle)/2,0,0]) translate([0,0,7.5+2]) cylinder(d=12,h=4,center=true); // cam lens extra clearance
        
    }
}

module CoverBase()
{
    hzyl = 11;
    dzyl = InnerCoverPillarDia;
    
    union() 
    {                  
        hull() // lower body
        {
            copy_mirror([0,1,0]) copy_mirror([1,0,0]) translate ([10, 10, 0]) cylinder(h=hzyl,d=dzyl, $fn=12);
        }
        copy_rotate([0,0,90]) copy_rotate([0,0,90]) copy_rotate([0,0,90]) 
        hull() // feet
        {
            translate ([10, 10, 0]) cylinder(h=hzyl,d=dzyl+2, $fn=12);
     
            rotate([0,0,-45]) translate([0,52.6/2+2.4,0]) cylinder(h=3,d=7, $fn=12);
        }
    }
}


module Binder(D= 5, th= 1.5, w= 2.5)
{
    difference()
    {
        cylinder(d=D+2*th,h=w,center=true);
        cylinder(d=D,h=w,center=true);
    }
}




module COVER()
{
    difference()
    {
        union()
        {
            CoverBase();
            minkowski()
            {
                innerCover(false);
                sphere(d=CoverThickness);
            }
            translate([0,18,7.5]) rotate([0,90,0]) cylinder(d=5,h=32,center=true); // front stiffener
            translate([0,-15.5,7.5]) rotate([0,90,0]) cylinder(d=5,h=31,center=true); // back stiffener
            copy_mirror([1,0,0])translate([-16.5,0,7.5]) rotate([90,0,0]) cylinder(d=5,h=31,center=true); // side stiffener
            
            translate(campos)  rotate([-90+(lower_angle+upper_angle)/2,0,0]) translate([0,0,7.5+2.75]) cylinder(d1=19,d2=19,h=8,center=true); // Protector ring
           
            hull() // antenna support
            {
                for(i=[lower_angle:5:upper_angle])
                {
                    translate(campos)  
                    rotate([i,0,0]) 
                    translate([3.5,-13+1+1,5+4])
                    cylinder(d1=21.5,d2=10,h=12);
                } 
            }      
            
        }   
        innerCover(true);
        STUFF(true); 
        translate([0,0,-2+0.001]) cube([100,100,4],true); // remove lower skin
        rotate([0,0,0])translate([0,-50,1.5])cube([8,100,3],center=true); // remove space for wiring - battery
        
        translate(campos) rotate([lower_angle,0,0]) translate([3.5,-13+1+1,14.5+4]) Binder();
        hull()
        {
            // cutout
            translate(campos) rotate([lower_angle,0,0]) translate([3.5,-13+1,5+4]) cylinder(d=3.0,h=15);
            translate(campos) rotate([lower_angle,0,0]) translate([3.5,-22+1,5+4]) cylinder(d=10,h=15);
        }
        translate(campos)  
                    rotate([lower_angle,0,0])
                    translate([3.5,-13+1,4])
                    cylinder(d1=24,d2=10,h=12);
        
        translate([0,-20,19]) rotate([90,0,0]) cylinder(d=20,h=25,center=true); // cut away back
    }
}

module FRAME()
{
    translate([0,0,-1])
    union()
    { 
        difference()
        {
            cube([37+10,37+10,2],center=true);
            union()
            {
                    rad=10;
                    copy_rotate([0,0,90]) copy_mirror([1,0,0]) translate([(36.7+rad*2)/2,0,0]) cylinder(r=rad,h=2.1,center=true);
            }
        
        }
        for(i=[0,90,180,270])
        {
            
            rotate([0,0,i+45]) 
            translate([50,0,0]) cube([70,20,2],center=true);
        }
    }
    
}


module STUFF(exp=false)
{
    translate([0,0,lowerstand]) rotate([0,0,-90]) ESC20x20(exp);
    translate([0,0,lowerstand+upperstand+1]) rotate([0,0,180]) OMNIBUS20x20(exp);

    union()
    {
        for(i=[lower_angle:7.5:upper_angle])
        {
            translate(campos)  rotate([-90-i,180,0]) 
            RUNCAM_SWIFT(exp);
        }       
    }
  
    translate([0,-8,16]) rotate([43,0,0]) BEEPER(exp);

    // has room there: 
    copy_mirror([1,0,0]) translate([-14,-1,20]) rotate([0,90,0]) RX_XMPLUS(false); // antenna goes low.

    if(exp)
    {
        for(i=[45:90:360])
        {
              rotate([0,0,i])  translate([75,0,0]) motor1807(exp);
        }
        color("grey") translate([0,0,1]) cube([60,16,2],center=true); // Velcro
   
        cylinder(d=18,h=15,center=false);
        copy_mirror([1,0,0]) translate ([13, 0, 1]) rotate ([90,0,0]) cylinder(h=50,d=5,center=true); // motor wire cutout
    }
}


if(isDemo)
{
    translate([0,0,0]) STUFF(false);
    //translate([0,0,0]) innerCover(true);       
    //FRAME();
    difference()
    {
        COVER();
        color("blue") /*rotate([0,0,45])*/ translate([-50+3.5,0,0]) cube([100,100,100],center=true); // half it
    }
    BODY(); 
}
else
{
    COVER();
    translate([50,0,0]) 
    BODY(); 
}
//rotate([-90-(lower_angle+upper_angle)/2,0,0])

!rotate([-lower_angle,0,0]) translate([-50,0,0]) 
difference()
{
    color("blue") translate(campos) rotate([lower_angle,0,0]) translate([3.5,-13+1+1,5+4+5]) cylinder(d1=17.5,d2=5,h=32);
    hull() // antenna support
            {
               translate(campos) rotate([lower_angle,0,0]) translate([3.5,-13+1+1,5+4]) cylinder(d1=21.5,d2=10,h=12);
                 
            }  
        hull()
        {
            // cutout
            translate(campos) rotate([lower_angle,0,0]) translate([3.5,-13+1,5+4]) cylinder(d=3.0,h=40);
            translate(campos) rotate([lower_angle,0,0]) translate([3.5,-22+1,5+4]) cylinder(d=10,h=40);
        }    
        translate(campos) rotate([lower_angle,0,0]) translate([3.5,-13+1+1,14.5+4+24]) Binder();
}


