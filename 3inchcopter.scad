/*
Indoor Copter
1103 motors

(c) 2018 Fabian Huslik

https://github.com/fabianhu/
*/ 

use <libCopterFCs.scad>
use <libCopterESCs.scad>
use <libCopterCams.scad>
use <libCopterTXs.scad>
use <libCopterMotors.scad>

$fn = 60;
radius = 3 * (25.4/2)+0.5; // propeller radius
echo("Prop radius: ",radius);
thick = 0.8; // rest body

th=3.5;

dx=radius*2+16;
dy=radius*2+6;

angF = 36+2;
angR = 39+2;

campos=[0,26,5+1.85];
camangle = 15;
FCpos = [0,0,0];
BattSize=[13,42+8,17.5];
BattPos = [0,-25,thick+BattSize.z/2-th]; // BattSize z !

// helper to morror while keeping the original.
module copy_mirror(vec=[0,1,0])
{
    children();
    mirror(vec) children();
}

module squarepattern(diff){
    translate([diff/2,diff/2,0]) children();
    translate([-diff/2,diff/2,0]) children();
    translate([diff/2,-diff/2,0]) children();
    translate([-diff/2,-diff/2,0]) children();
}

module esc16(){
    cube([29.5,25,3.5],center=true);
}


module txcam(){
    translate([0,0,-9/2])cube([15,12.5,9],center= true);
    cylinder(d=10.2,h=16.5-9);
    translate([-2,-17/2-2.25,-9]) cube([7,17,9]);
}


module STUFF(exp=false)
{
    translate(campos) rotate([-90+camangle,0,0]) 
    union()
    {
        txcam();
        
    }

        
    translate([0,0,7]) translate(FCpos) rotate([0,-0,0]) REVO16x16(exp);
    translate([0,0,3+1.0]) translate(FCpos) rotate([0,-0,0]) esc16();
    translate([0,0,10]) rotate([0,0,0]) RX_XM(exp);

    
     if(exp)
     {   
        copy_mirror([1,0,0]) translate([dx/2,dy/2,0]) rotate([0,0,180+angF]) motor1103_3(exp);
        copy_mirror([1,0,0]) translate([dx/2,-dy/2,0]) rotate([0,0,180-angR]) motor1103_3(exp);
         
         translate(FCpos) translate([0,0,-th]) squarepattern(16) cylinder(d=1.7,h=10);
     } 
}



module Battery(){
    BattRad=1.5;
    minkowski(){
        translate([0,-th,0])cube(BattSize-[BattRad,BattRad,BattRad],center=true);
        sphere(BattRad/2);
    }
}


module BattHld()
{
    difference()
    {
        minkowski(){
            Battery();
            sphere(thick);
        }
        Battery();
        translate([0,-21-5,-0.5]) cube([15,12,21],center=true); // minus end
        
        // up-dn holes
        translate([0,15,0]) cylinder(d=10,h=20,center=true);        
        translate([0,1,0])cylinder(d=10,h=20,center=true);
        translate([0,-13,0])cylinder(d=10,h=20,center=true);
        
        
        translate([0,15,0]) rotate ([90,0,0]) cylinder(d=10,h=20,center=true); // front end
        
        translate([0,2,0])rotate ([90,0,90])cylinder(d=12,h=20,center=true);
        translate([0,-12,0])rotate ([90,0,90])cylinder(d=12,h=20,center=true);
    }
    
}

module arm(){
    translate([0,0,-th])cylinder(d1=11.5,d2=12.5,h=th); // outer holder
        
    translate([0,0,-th]) rotate([0,0,180]) hull(){
        cylinder(d1=5, d2=3,h=th);
        translate([0,55+2,0]) cylinder(d1=10, d2=5,h=th);
    }
}


module body()
{
    difference()
    {
        union(){
            copy_mirror([1,0,0]) translate([dx/2,dy/2,0]) rotate([0,0,-90+angF]) arm();
            copy_mirror([1,0,0]) translate([dx/2,-dy/2,0]) rotate([0,0,-90-angR]) arm();
            //copy_mirror([0,1,0]) 
            
            hull(){
                translate([0,0,-th/2]) cube([14,16,th],center = true); // center piece
                //translate([0,-13,-th]) rotate([camangle,0,0])cylinder(d=3,h=4);
            }
            
            hull(){    
            #translate([FCpos.x,FCpos.y+9,-th/2]) cube([10,1,th],center=true);
            
            translate(campos) rotate([-90+camangle,0,0]) translate([-8,-2+5,-10]) cube([16,5,10]);
            }
            
         }
         //translate(BattPos) Battery();
         
         //translate([0,-13,-th]) rotate([camangle,0,0])cylinder(d=2,h=4);
        
    }
    
    //translate(BattPos)BattHld();
    
    translate(FCpos) fcholder();


    


}

module fcholder(){
    translate([0,0,-th])
    difference(){
        union(){
            hull(){
                translate([8,8,0])cylinder(d=4.0,h=th);
                translate([-8,-8,0])cylinder(d=4.0,h=th);
            }
            hull(){
                translate([-8,8,0])cylinder(d=4.0,h=th);
                translate([8,-8,0])cylinder(d=4.0,h=th);
            }
            translate([0,0,th])squarepattern(16) cylinder(d1=4.0,d2=3.5,h=2.5);
        }
        squarepattern(16) cylinder(d=1.7,h=8);
        squarepattern(16) translate([0,0,th])cylinder(d=2.1,h=th);
    }
}


difference()
{
body();
STUFF(true);   

}  


//STUFF(true);   
