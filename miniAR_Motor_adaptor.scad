// mini AR Wing motor adaptor for 1605
$fn=60;
module bolt(m=3){
    translate([0,0,-20]) cylinder(d=m,h=21);
    translate([0,0,0]) cylinder(d=m+2.5,h=8);
    translate([0,0,0]) cylinder(d=m,h=20);
}

module PlaneBolts(){
    dxy = 21;
    dz = 1.5;
    translate([dxy/2,dxy/2,dz]) bolt();
    translate([-dxy/2,dxy/2,dz]) bolt();
    translate([dxy/2,-dxy/2,dz]) bolt();
    translate([-dxy/2,-dxy/2,dz]) bolt();
}

module MotorBolts(){
    dx = 16;
    dy = 12;
    dz = 2.5;
    translate([dx/2,0,dz]) bolt(2.5);
    translate([-dx/2,0,dz]) bolt(2.5);
    //translate([dy/2,0,dz]) bolt(2.5); // extra double hole
    //translate([-dy/2,0,dz]) bolt(2.5);
    translate([0,dy/2,dz]) bolt(2.5);
    translate([0,-dy/2,dz]) bolt(2.5);
    translate([0,0,-1])cylinder(d=7.5,h=dz+2);
    
}

module CoolHoles(he=21){
    dxy = 25;
    m=12;
    h=he+1;
    hull(){
        translate([0,dxy/2,0]) cylinder(d=m,h=0.1);
        translate([xoff+0,dxy/2,h]) cylinder(d=m-2,h=0.1);
    }
    hull(){
        translate([-dxy/2,0,0]) cylinder(d=m,h=0.1);
        translate([xoff+0-dxy/2,0,h]) cylinder(d=m-2,h=0.1);
    }
    hull(){
        translate([dxy/2,0,0]) cylinder(d=m,h=0.1);
        translate([xoff+dxy/2,0,h]) cylinder(d=m-2,h=0.1);
    }
    hull(){
        translate([0,-dxy/2,0]) cylinder(d=m,h=0.1);
        translate([xoff+0,-dxy/2,h]) cylinder(d=m-2,h=0.1);
    }
}

he = 10;

//translate([0,0,he+4]) cylinder(d=21,h=10);
//translate([0,0,he]) cylinder(d=16,h=4);

xoff =4;

module carrierhole(){
    rad=4;
    dx=33.5/2-rad;
    dy=33/2-rad;
    t=3.25;

    translate([0,0,-t])cylinder(d=12,h=t);
    translate([-6,0,-t/2]) cube([16,12,t],true);

}


module carrierhull(){
    rad=4;
    dx=33.5/2-rad;
    dy=33/2-rad;
    t=3;
    hull(){
        translate([dx,dy,-t])cylinder(r=4,h=t);
        translate([-dx,dy,-t])cylinder(r=4,h=t);
        translate([dx,-dy,-t])cylinder(r=4,h=t);
        translate([-dx,-dy,-t])cylinder(r=4,h=t);
    }
}


difference(){
    union(){
        carrierhull();
        hull(){
            cylinder(d=42-2,h=.1);
            
            translate([xoff,0,he]) cylinder(d=24,h=0.1);
        }
    }
        hull(){
        carrierhole();
        translate([xoff+0-25/2,0,he]) cylinder(d=10,h=0.1); // one coolhole
    }
    
    india=30;
    translate([0,0,-3])cylinder(d1=12,d2=india,h=3);
    //PlaneBolts();
    translate([xoff,0,he]) rotate([180,0,45])MotorBolts();
    CoolHoles(he);

    hull(){
        cylinder(d=india,h=0.1);        
        translate([xoff,0,he-2.5-0.1]) cylinder(d=14,h=0.1);
    }
}

