// DF Spirit Wing motor adaptor for 1204
$fn=60;
module bolt(m=2){
    translate([0,0,-12]) cylinder(d=m,h=12);
    translate([0,0,0]) cylinder(d=m+2.5,h=5);
    //translate([0,0,0]) cylinder(d=m,h=21);
}

module MotorBolts(){
    dxy = 6.5;
    dz = 3;
    translate([dxy/2,dxy/2,dz]) bolt();
    translate([-dxy/2,dxy/2,dz]) bolt();
    translate([dxy/2,-dxy/2,dz]) bolt();
    translate([-dxy/2,-dxy/2,dz]) bolt();
}

ro = [0,45,30];
tn = [7,10,5];

difference(){
    union(){
    
        translate([-10,0,0])cube([20,8,2.5]);
        translate([-10,1.5,0])cube([20,7,2.5+10]);
        hull(){
        translate([0,10,0]) cylinder(d=15,h=2.5+10);
    
        translate([0,10,0]) rotate([0,0,-45]) cube([7.5+1,7.5,4]);
        mirror([1,0,0])translate([0,10,0]) rotate([0,0,-45]) cube([7.5+1,7.5,4]);
            
            
            }
            //translate(tn) rotate(ro) cylinder(d=8,h=6,center=true);
    }
    translate([0,10,5]) cylinder(d=5,h=10);
    translate([0,4.5,5]) cylinder(d=4,h=10);
    translate([0,10,12.5-2]) rotate([180,0,0]) MotorBolts();
    
    hi = 9.5-2;
    translate([0,10,-0.01]) cylinder(d=12,h=hi);
    translate([0,10,10.5]) cylinder(d=13.4,h=3.5);
    
    translate([-8,2.5,-0.01])cube([16,8,hi]);
    
    //cube(30);
    translate(tn) rotate(ro) cylinder(d=6,h=10,center=true);
    mirror([1,0,0]) translate(tn) rotate(ro) cylinder(d=6,h=12,center=true);
    
}