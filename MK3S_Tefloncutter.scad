use <threads.scad>

$fn=120;
// metric_thread(diameter=5,pitch=0.8,length=12, internal=true);
// metric_thread(diameter=4,pitch=0.7,length=12, internal=true);
// metric_thread(diameter=8,pitch=1.25,length=12, internal=true);
// cylinder(d=4, h=6);


pdia = 4.1;
plen = 44.3; // original length of MK3S tube according drawing
pang = 45;

poff = 10;

module dtr(){
   metric_thread(diameter=4,pitch=0.7,length=9, internal=true);
}

difference(){
    translate([poff/2,0,0]) cube([plen+poff,16,16],center=true);

    rotate([0,90,0]) cylinder(d=pdia, h=100, center=true);
    
    translate([plen/2,0,0])rotate([0,90,0]) metric_thread(diameter=8,pitch=1.25,length=poff+1, internal=true);
    
    rotate([0,0,0]) dtr();
    rotate([90,0,0]) dtr();
    rotate([180,0,0]) dtr();
    rotate([270,0,0]) dtr();
    
    translate([-plen/2,0,0]) rotate([0,0,-pang]) translate([-100,0,-50]) cube(100);
    translate([-plen/2,0,0]) rotate([0,0,pang+90]) translate([-100,0,-50]) cube(100);
    

    
}

%difference(){
    color("green")rotate([0,90,0]) cylinder(d=pdia, h=plen+2, center=true);

    bonddiam = 8;
    bondoffs =-2.5;
    translate([-plen/2-bondoffs-bonddiam/2,bonddiam/2,-5])cylinder(d=bonddiam, h= 10);
    translate([-plen/2-bondoffs-bonddiam/2,-bonddiam/2,-5])cylinder(d=bonddiam, h= 10);
}
