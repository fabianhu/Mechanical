$fn=60;

use <libCopterFCs.scad>

module kbb(r = 3)
{
    w = 2;
    d = 1;
    translate([0,0,r])rotate([90,0,0])difference(){
        cylinder(r=r,h=w,center = true);
        cylinder(r=r-d,h=w,center = true);
    }
}

module squarify(x){
    translate([+x/2,+x/2,0])children();
    translate([-x/2,+x/2,0])children();
    translate([+x/2,-x/2,0])children();
    translate([-x/2,-x/2,0])children();
}



module socket(){
    translate([0,-14,5]) cube([10.5,9,10],center=true);
}

module fc(){
    translate([0,0,2.3+.5]) OMNIBUS20x20(true);
}

module bottomdings(){ // old for mini AR wing
difference(){
    hull(){
        blockH = 2.3;
        translate([15+1,15+2,0]) cylinder(d=6,h=blockH);
        translate([15+1,-15-2,0]) cylinder(d=6,h=blockH); 
        translate([-15-1,-15-2,0]) cylinder(d=6,h=blockH); 
        translate([-15-1,15+2,0]) cylinder(d=6,h=blockH); 

        }
    //translate([0,5,0])  cylinder(d1=30,d2=20,h=5);
    
        socket();
        fc();

    translate([0,5,-1]) union(){
        translate([10,10,0]) cylinder(d=1.6,h=5); 
        translate([10,-10,0]) cylinder(d=1.6,h=5); 
        translate([-10,-10,0]) cylinder(d=1.6,h=5); 
        translate([-10,10,0]) cylinder(d=1.6,h=5); 
        }

    translate([18,15,0]) kbb(); 
    translate([18,-15,0]) kbb(); 
    translate([-18,-15,0]) kbb(); 
    translate([-18,15,0]) kbb(); 
    translate([18,5,0]) kbb(); 
    translate([-18,5,0]) kbb(); 
    translate([18,-5,0]) kbb(); 
    translate([-18,-5,0]) kbb(); 
        
    translate([10,-19,0]) rotate([0,0,90])kbb(); 
    translate([-10,-19,0]) rotate([0,0,90])kbb(); 

    }
translate([15,15,-1]) cylinder(d=5.6,h=1,$fn=6); 
translate([15,-15,-1]) cylinder(d=5.6,h=1,$fn=6); 
translate([-15,-15,-1]) cylinder(d=5.6,h=1,$fn=6); 
translate([-15,15,-1]) cylinder(d=5.6,h=1,$fn=6); 
}

    //fc();    color("red") socket();
    
    
module topdings(){
    translate([0,0,10])
difference(){
    hull(){
        blockH = 2.0;
        translate([10,10,0]) cylinder(d=6,h=blockH);
        translate([10,-10,0]) cylinder(d=6,h=blockH); 
        translate([-10,-10,0]) cylinder(d=6,h=blockH); 
        translate([-10,10,0]) cylinder(d=6,h=blockH); 

    }
        
    translate([0,0,-1]) union(){
        translate([10,10,0]) cylinder(d=2.2,h=5); 
        translate([10,-10,0]) cylinder(d=2.2,h=5); 
        translate([-10,-10,0]) cylinder(d=2.2,h=5); 
        translate([-10,10,0]) cylinder(d=2.2,h=5); 
    }
    
    translate([0,0,1]) union(){
        translate([10,10,0]) cylinder(d=4,h=5); 
        translate([10,-10,0]) cylinder(d=4,h=5); 
        translate([-10,-10,0]) cylinder(d=4,h=5); 
        translate([-10,10,0]) cylinder(d=4,h=5); 
    }
        
    translate([13,5,-0]) kbb(); 
    translate([13,-5,0]) kbb(); 
    translate([-13,-5,0]) kbb(); 
    translate([-13,5,0]) kbb(); 
    
    translate([5,5,-0]) kbb(10); 
    translate([5,-5,0]) kbb(10); 
    translate([-5,-5,0]) kbb(10); 
    translate([-5,5,0]) kbb(10); 
    
    rotate ([0,0,90])union(){translate([13,5,-0]) kbb(); 
    translate([13,-5,0]) kbb(); 
    translate([-13,-5,0]) kbb(); 
    translate([-13,5,0]) kbb(); 
    }
        
    }
}


module nubsi(h){
    cylinder(d=4.5,h=h);
    cylinder(d1=6,d2=4.5,h=1);
}

module bottomsimple(){
difference(){
    union()
    {
        hull(){
            blockH = 0.5;
            blockdia=6;
            //squarify(20) cylinder(d=blockdia,h=blockH);
            translate([5.5/2,5.5/2,0])squarify(25.5) cylinder(d=blockdia,h=blockH);
        }
        squarify(20) nubsi(2.5);
        translate([5.5/2,5.5/2,0])squarify(25.5) cylinder(d=4.5,h=2.5+5);;
    }
    
    cylinder(d=16,h=2);
    
    #fc();
    
    translate([0,0,-1]) union(){
        drilldia=1.75;
        squarify(20) cylinder(d=drilldia,h=8);
        translate([5.5/2,5.5/2,0])squarify(25.5) cylinder(d=drilldia,h=10); 
    }
}
}

translate([-50,10,0])
squarify(20)
difference(){
    cylinder(d=4.5,h=3);
    cylinder(d=2.1,h=3);
}

//fc();
bottomsimple();
translate([100,0,0])bottomdings();
translate([100,0,0])topdings();