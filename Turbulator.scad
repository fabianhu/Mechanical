
 w= 6.5;

cube([270,w,0.1]);
 h=1.4;

 t=0.85;
 l= 4.5;
 r=55;
 
 
 module minicube(){
     hull(){
         translate([0,0,h/2]) cube([l,t,h],true);
         translate([0,0,0.3/2])cube([l+1,t+1,0.3],true);
     }
     translate([0,0,0.3/2])cube([l+2,t+2,0.3],true);
 }

step=14; 

for(i=[5:step:265]){
   translate([i,w/2,0]) 
    union(){
        rotate([0,0,0])minicube();
        translate([step/2,0,0])rotate([0,0,90])minicube();
    }
}