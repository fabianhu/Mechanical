use <libCopterMotors.scad>;
use <libCopterFCs.scad>
use <libCopterESCs.scad>
use <libCopterCams.scad>
use <libCopterTXs.scad>

// prop profile
dp=3*25.6;
ba=.6*dp;
a=.4*ba;
b=.18*ba;

// cassis
l = 1.2*dp/2;
xw = 6;      // x width
yw = 3;

$fn=40;
notime = false;
stuff = true;

module copy_mirror(vec=[0,1,0])
{
    children();
    mirror(vec) children();
}

module copy_rotate(vec=[0,90,0])
{
    children();
    rotate(vec) children();
}

module fan(){

    rotate([180,0,0])
    rotate_extrude(angle=360, convexity=2)
    rotate([0,0,100])translate([0,-(dp/2+b/2+2),00])
    polygon(points=[[-12.3,0.0],[-11.3,1.31],[-10.3,1.81],[-9.29,2.17],[-8.29,2.45],[-7.29,2.67],[-6.29,2.85],[-5.29,2.99],[-4.29,3.11],[-3.29,3.2],[-2.29,3.26],[-1.29,3.3],[-0.288,3.32],[0.712,3.31],[1.71,3.27],[2.71,3.21],[3.71,3.13],[4.71,3.02],[5.71,2.88],[6.71,2.73],[7.71,2.56],[8.71,2.37],[9.71,2.17],[10.7,1.96],[11.7,1.73],[12.7,1.49],[13.7,1.24],[17.7,0.193],[17.7,-0.00488],[16.7,-0.0268],[15.7,-0.0645],[14.7,-0.116],[13.7,-0.179],[12.7,-0.251],[11.7,-0.331],[10.7,-0.416],[9.71,-0.505],[8.71,-0.594],[7.71,-0.683],[6.71,-0.768],[5.71,-0.848],[4.71,-0.921],[3.71,-0.985],[2.71,-1.04],[1.71,-1.08],[0.712,-1.1],[-0.288,-1.11],[-1.29,-1.1],[-2.29,-1.09],[-3.29,-1.07],[-4.29,-1.04],[-5.29,-0.998],[-6.29,-0.95],[-7.29,-0.89],[-8.29,-0.816],[-9.29,-0.724],[-10.3,-0.605],[-11.3,-0.437],[-12.3,0.0],[-12.3,0.0]]);

    //color("red")translate([0,0,-10])cylinder(d=dp, h = 4, center=true);

    rs = 120;

    difference(){
        union(){
        intersection(){        
            translate([0,0,-rs+13.7])
            difference(){
                sphere(r=rs);
                sphere(r=rs-3);
                translate([0,0,-50])cube([2*rs,2*rs,2*rs], center = true);
                
                for(i=[0:120:360])
                {
                    rotate([0,0,i])
                    union(){
                        translate([dp/2+2,dp/2+15.5,rs])cube([dp, dp, dp/2], center=true);
                        translate([-dp/2-2,dp/2+15.5,rs])cube([dp, dp, dp/2], center=true);
                    }
                    rotate([0,0,i+30])translate([dp-9, 0, rs])scale([1.6,1,1])cylinder(r=dp/2, h =20, center=true);
                }
            }
            cylinder(d=1.2*dp, h= 80, center=true);
        }
        translate([0,0,11.5])cylinder(r=8.8,h=3,center=true);
        }
        translate([0,0,10])rotate([180,0,-30])intersection(){
            motor1306(true);
            cylinder(r=dp/3, h = 50, center=true);
        }
    }
    if (stuff)
        translate([0,0,10])rotate([180,0,-30]) motor1306(true);
    //translate([0,0,11])rotate([180,0,-30])motor1306(false);

}

module fans(){
        
 
    translate([l+yw,l+xw,0])fan();
    translate([-(l+yw),l+xw,0])fan();
    translate([l+yw,-(l+xw),0])fan();
    translate([-(l+yw),-(l+xw),0])fan();
}

module CONNECTOR(ws=5, we=8, thickness = 3, height=20){    
        
    for(i = [0:1:thickness])
    {
        translate([i,0,0])
        cube([1, ws+((we-ws)/thickness)*i, height],center = true);
        //cube([1, i, height],center = true);
    }
}

module STUFF(exp){
    lowerstand = -10;
    upperstand = 6;
    
    // LIPO
    // https://hobbyking.com/de_de/turnigy-650mah-3s-65c-lipo-pack-w-xt30.html
    color("grey") translate([0,0,-27]) cube([ 31, 56, 20 ], center=true); 
    
    // FC/ESC
    translate([0,0,lowerstand]) rotate([0,0,-90]) ESC20x20(exp);
    translate([0,0,lowerstand+upperstand+1]) rotate([0,0,180]) OMNIBUS20x20(exp);
    
    // TX 
    // color("grey") translate([0,0,lowerstand+2*upperstand+1])cube([14.5,12,9],true);
    
    // CAM
    translate([0,22,7])rotate([-55,0,0])RUNCAM_NANO(true);
}





difference(){
    copy_mirror() translate([0,l,-23])rotate([0,-90,90])CONNECTOR(17,9,19,5);
    translate([0,0,-26])rotate([90,0,0])scale([1,2.5])cylinder(r=6, h = 250, center=true);
}

difference(){
copy_mirror([1,0,0])
translate([l,0,-23])rotate([0,-90,0])CONNECTOR(23,13,19,5);
translate([0,0,-26])rotate([0,90,0])scale([2.5,1.5])cylinder(r=6, h = 250, center=true);
}

difference(){
    union(){        
        fans();
        difference(){
            translate([0,0,-20]) cube([ 31+4, 56, 10 ], center=true);
            if (notime){
            copy_mirror() 
            hull(){
                translate([0,-22,-16])cylinder(r=10, h = 5, center=true);
                translate([0,-8,-16])cylinder(r= 2, h = 5, center=true);
            }
            copy_mirror([1,0,0]) translate([17,0,-16])cylinder(d=20, h = 25, center = true);
        }
        }
  
    }
    STUFF();
    color("red")translate([0,0,-25])cube([250,250,3],center=true);
}
if(stuff)
    STUFF(true);

