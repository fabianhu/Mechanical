$fn=30;
rad = 2.5;

hull(){
    translate([66/2-rad,rad,0])cylinder(r=rad,h=10);
    translate([-66/2+rad,rad,0])cylinder(r=rad,h=10);
    
    translate([66/2-rad,80,0])cylinder(r=rad,h=10);
    translate([-66/2+rad,80,0])cylinder(r=rad,h=10);
    
    translate([49/2-rad,107-rad,0])cylinder(r=rad,h=10);
    translate([-49/2+rad,107-rad,0])cylinder(r=rad,h=10);
}

