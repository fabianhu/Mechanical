/*
90 mm Copter

(c) 2018-2020 Fabian Huslik

https://gitlab.com/fabianhu/
*/ 

use <libCopterFCs.scad>
use <libCopterESCs.scad>
use <libCopterCams.scad>
use <libCopterTXs.scad>
use <libCopterMotors.scad>

$fn=30;
camangle = 20;
campos = [0,21-3,14+3.5+4];
RCNANO = true;  // is Runcam nano or cheapo cam
PRINT = true;  // true stands on its face for better printing
lefty = true; // the controller is installed backwards

module copy_mirror(vec=[0,1,0])
{
    children();
    mirror(vec) children();
}

module copy_rotate(vec=[0,90,0])
{
    children();
    rotate(vec) children();
}




module Standoffs (sq,h)
{
    difference()
	{     
			copy_mirror(vec=[1,0,0]) copy_mirror(vec=[0,1,0]) translate ( [  sq/2,   sq/2,  0]) cylinder(h,d=4, $fn=30);
			copy_mirror(vec=[1,0,0]) copy_mirror(vec=[0,1,0]) translate ( [  sq/2,   sq/2,  0]) cylinder(h,d=2.1, $fn=30);
    }

}


module BODY() {
	l = 20/2;
	lb= 27;
	wb= 31;
	h=10.5+1;
	r= 6;
	w= 10;
	d = 10;
	difference()
    {
		union()
        {
			translate([0,0,7.5+0.5])
					//center body hull
			hull()
            {
					copy_mirror(vec=[1,0,0]) copy_mirror(vec=[0,1,0])translate ( [  w,   d,  0]) cylinder(h,d=r, $fn=30);
			}

			// antenna support hull
			hull()
			{
				//translate([-1,-11,16])rotate([90,0,0]) cylinder(h=11,d=4); 
                translate([0,-15-1,16]) cube([9,5,5],center=true);

				for(i = [ [  w,   -d,  3.5+4.5+4.5],  [-w,  -d, 3.5+4.5+4.5],  ])
				{
					translate (i) cylinder(h=6,d=r, $fn=30);    
				}
			}

			// cam protector hull
			hull()
			{
				translate(campos)  rotate([-90+camangle,0,0]) translate([0,0,2.5-0.2]) 
            		//cube([12,12,5],true);
				
                if(RCNANO) 
                {cylinder(d1=16,d2=14.5,h=18,center=true);} 
                else 
                {cylinder(d=14.5,h=9.25,center=true);}
				

				for(i = [ [  w,   d,  3.5],  [-w,  d, 3.5],  ])
				{
					translate (i) cylinder(h=15.0,d=r, $fn=30);    
				}
			}

            copy_mirror([1,0,0]) RXAntPos() cylinder(d=4,h=6); 

		} // union main body

		STUFF(true);

		// holes
		for(i = [ 
		         [  10,   10,  0],
		         [ 10, -10, 0],
		         [-10,  10, 0],
		         [-10,  -10, 0] ])
		{
			translate (i) cylinder(8+5,d=1.8, $fn=30);
			translate (i) cylinder(8.5,d=2.2, $fn=30);
		}

		cylinder(h=15,d=24); // center hole cutout

        hq=9.5;
        //translate([0,21,hq/2])cube([14.5,15,hq],center=true);
        
    

	}


}

module AntHolderBolts()
{
    translate([0,-12.5,15.5])
    union()
    {
        copy_mirror([1,0,0]) rotate([90,0,0]) translate([2.5,0,0])  cylinder(h=5,d=1.75); 
        copy_mirror([1,0,0]) rotate([90,0,0]) translate([2.5,0,5])  cylinder(h=7,d=2.1);
        copy_mirror([1,0,0]) rotate([90,0,0]) translate([2.5,0,12])  cylinder(h=25,d=3.8);
    }
}

module AntHolder()
{
     difference()
    {
       color("blue")
        union()
        {
            translate([0,-20,16]) cube([9,5,5],center=true);
            
            translate([0,-18,36])
            
            rotate([0,-90,0])
            rotate_extrude(angle=90-camangle,$fn=60)
            translate ([-20,0]) difference()
            {
                circle(4);
                translate([2,0]) circle(1.25);
                translate([7,0]) square(10,center=true);
            }
            
        }
        translate([0,-13-4.5,18]) rotate([90,0,0]) cylinder(h=5,d=2.5); 
        AntHolderBolts();
        translate([0,-50,25]) rotate([15,0,0]) cube(30,center=true);
        translate([0,-30,25]) rotate([camangle,0,0]) copy_mirror([1,0,0]) rotate([90,0,0]) translate([2.75,0,0])  cylinder(h=5,d=2); 
        translate([0,-25,-0.5])  cube(28,center=true);
    }

}


module RXAntPos()
{
translate([4.5-0.5,-14+0.5,15]) 
    rotate([camangle,30,0]) 
    children();   
}



module STUFF(clr=false)
{
	translate([0,0,2.5])  rotate([0,0,-90]) ESC20x20(clr);
	if(lefty){
         translate([0,0,7.2]) rotate([0,0,180])OMNIBUS20x20(clr);
         translate([-13.5,-2,17])  rotate([0,0,0])     BEEPER(false);
    }
    else{
        translate([0,0,7.2]) OMNIBUS20x20(clr);
        translate([-13.5,0,14])  rotate([0,0,0])     BEEPER(false);
    }
	translate([2,-2,11.75]) rotate([0,180,0])RX_XMPLUS(clr);

	if(RCNANO)
	{translate(campos)  rotate([-90+camangle,0,0]) RUNCAM_NANO(clr);
        translate(campos)  rotate([-45,0,0]) translate([0,-2,-12])  cube([14.5,14.5,16],true); // cam install space
        translate(campos)  rotate([-90+camangle,0,0]) translate([0,-10,-5])  cube([14.5,10,10],true); // top cutaway
        }
	else
	{
        translate(campos)  rotate([-90+camangle,0,0]) CAMERA800(clr);
        translate(campos) rotate([camangle,0,0]) translate([0,-7-1,0])cube([14,14,13],true);  // cam mounting cutout 
    }
	

	translate([1,1,15.5])  rotate([0,0,180])TX03(clr);

    
    copy_mirror([0,1,0]) copy_mirror([1,0,0]) translate([63/2,63/2,0]) motor1104(clr);
    
    AntHolderBolts();
    
        copy_mirror([1,0,0]) RXAntPos() 
        union()
        {
        translate([0,0,-5])cylinder(d1=3,d2=1.5,h=5+1); 
        translate([0,0,1])cylinder(d=2,h=60); 
        }
    
    
}

//STUFF(false);

//rotate([PRINT? -camangle-90:0,0,0])
BODY();  

//rotate([PRINT? -90:0,0,0])
translate([PRINT? -30:0,0,0]) 
AntHolder();

translate([PRINT? 30:0,0,0.3]) Standoffs(20,2.3);
translate([PRINT? 40:0,0,2.5+1])  Standoffs(20,3.7);
