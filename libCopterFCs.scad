/*
Copter parts Flight Controller library

(c) 2018 Fabian Huslik

https://github.com/fabianhu/
*/ 


translate([0,0,0]) REVO16x16(); 
translate([40,0,0]) OMNIBUS20x20();
translate([85,0,0]) RX_XM();
translate([100,0,0]) RX_XMPLUS();
translate([140,0,0]) BEEPER();

translate([0,40,0]) REVO16x16(true); 
translate([40,40,0]) OMNIBUS20x20(true);
translate([85,40,0]) RX_XM(true);
translate([100,40,0]) RX_XMPLUS(true);
translate([140,40,0]) BEEPER(true);



module REVO16x16(clr=false) 
{
    l = 16;
	PCB_x = 20.3;
    PCB_y = 20.3;
    PCB_r = 3.5;
	h=1; // PCB thickness
	
    difference()
    {
        
		color("green") hull()
        {
			for(i = [ 
			         [ (PCB_x-PCB_r)/2,   (PCB_y-PCB_r)/2,  0],
			         [ (PCB_x-PCB_r)/2, -(PCB_y-PCB_r)/2, 0],
			         [ -(PCB_x-PCB_r)/2,  (PCB_y-PCB_r)/2, 0],
			         [ -(PCB_x-PCB_r)/2,  -(PCB_y-PCB_r)/2, 0] ])
			{
				translate (i) cylinder(h,r=PCB_r/2,$fn = 12);
			}
		}

		for(i = [ [  l/2,   l/2,  0], [ l/2, -l/2, 0], [-l/2,  l/2, 0], [-l/2,  -l/2, 0] ])
		{   
			translate (i) cylinder(h,d=2.2,$fn = 12);
		}
	}
	
	color("grey") translate([0,0,h/2]) cube([12.8,19,4.2],true); // inner block
	color("grey") translate([0,0,h/2]) cube([19,12.8,4.2],true); // inner block
	color("grey") translate([8.0,0,(2.7/2)+h]) cube([6,7.8,2.9],true); // USB
    //color("lightgrey") translate([0,8,(2.7/2)+h]) cube([8.5,6,4.0],true); // ESC conn
    
    if(clr)
    {
        color("red") translate([14.0,0,(2.7/2)+h]) cube([6,8,2.9],true); // USB connector
        color("red") translate([-9,0,0.75+h]) cube([3,13,2],true); // upper connector space 
        color("red") translate([0,-9,0.75+h]) cube([13,3,2],true); // connector space
       // color("red") translate([0,13,(2.7/2)+h]) cube([8.5,4,3.5],true); // ESC conn 
    
        for(i = [ [  l/2,   l/2,  0], [ l/2, -l/2, 0], [-l/2,  l/2, 0], [-l/2,  -l/2, 0] ])
        {   
			//color("red") translate (i) cylinder(h=1.6,d=4);
		}
    }
    
}


module copy_mirror(vec=[0,1,0])
{
    children();
    mirror(vec) children();
}


module OMNIBUS20x20(clr=false) 
{


	l= 20/2;
	h=1.0; // PCB thickness
	difference()
    {

		color("green") hull()
        {
			for(i = [ 
			         [  l,   l,  0],
			         [ l, -l, 0],
			         [-l,  l, 0],
			         [-l,  -l, 0] ])
			{
				translate (i) cylinder(h,d=(27-20));
			}
		}

		for(i = [ 
		         [  l,   l,  0],
		         [ l, -l, 0],
		         [-l,  l, 0],
		         [-l,  -l, 0] ])
		{   
			translate (i) cylinder(h,d=2.2);
		}
	}
	thick= clr?5.6:1.8+h+1.8;
	color("grey") translate([0,0,h/2]) cube([16.5,27,thick],true); // inner block
	color("grey") translate([0,0,h/2]) cube([27,16.5,thick],true); // inner block
	color("grey") translate([11.5,0,(3/2)+h]) cube([6,7.8,3],true); // USB
    color("grey") translate([0,-12,(2.8/2)+h]) cube([12.5,7,3.7],true); // Conn
    
    if(clr)
    {
        color("red") translate([11.5+5,0,(2.8/2)+h]) cube([6+5,8,3.6],true); // USB
    }
    
}


module RX_XMPLUS(clr=false)
{
	
    color("grey") cube([12,21.9,3.5],true);
	color("red") if(clr)
    {
		color("red") cube([12.5,22,4],true);
        hull()
        {
			translate([1.5,-10,-1]) rotate([90,0,0]) cylinder(h=15,d=1.5);
			translate([5,-10,-1]) rotate([90,0,0]) cylinder(h=15,d=1.5);
		}
	}
}

module RX_XM(clr=false)
{
	
    color("grey") cube([10.3,16.9,3.5],true);
    color("grey") translate([0,-5,-1]) rotate([90,0,0]) cylinder(h=15,d=1.5);
	color("red") if(clr)
    {
			color("red") cube([12,18,3.8],true);
	}
}


module BEEPER(clr=false)
{
	color("grey") hull()
    {
        cylinder(d=9.2,h=6);
        translate([0,0,-1]) cylinder(d=6,h=1);
    }
	color("red") if(clr)
    {
        translate([0,0,-2]) cylinder(d=6,h=2);
        translate([0,0,6]) cylinder(d=9.5,h=2);
	}
	else
	{
		
	 
	}
}

