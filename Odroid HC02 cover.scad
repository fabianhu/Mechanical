// Odroid HC2 end cover

module odroidHCx(){
    translate([0,0,-1]) cube([56,46,1]); //board
    //cube([56,46,3]); //parts
    
    translate([37.8,-1.3,0]) 
    union(){
        cube([16.0,21.6,13.6]); //Net
        translate([2,-10,3.5]) cube([12.5,10,9.5]); //Net conn
    }
    
    translate([28.6,-1.5,-0.5]) 
    union(){
        cube([7.3,19.5,14.7]); //Usb
        translate([1,-12.5,0.5]) cube([5.3,12.5,13.7]); //Usb conn     
    }

    translate([19,-1.5,0]) 
    union(){
        cube([7.5,11.6,10]); //Pwr 
        //translate([31.5+0.5,-12.5,-0.5+3.5]) cube([6.5,12.5,6.5]); //Pwr conn
        translate([7.5/2,0,6.3]) rotate([90,0,0])cylinder(d=6,h=15); //Pwr conn
        translate([7.5/2,-2,6.3]) rotate([90,0,0])cylinder(d=9,h=15); //Pwr conn
    }
    
    translate([5.6,0,0]) 
    union(){
        cube([11,10,3]); //uSD
        translate([0,-7,1]) cube([11,7.5,2]); //uSD
    }
}


module HC2(){
    
    translate([7,-0.8,-0.5]) 
    minkowski(){
    odroidHCx();
    sphere(0.4);
        }
    
    translate([0,0,-7]) cube([103,10,7]); // lower profile

    //translate([-4.35,0,-5]) cube([111,10,23]);

}

module cover(){
    hull(){
    translate([0,-2,0]) cube([103,2,18+2.0]); // front covfefe
    
    
    translate([10,-4.5,-5]) cube([52,4.5,20]); // reenforcement covfefe
    }
    
    wup = 107.6;
    translate([-(wup-103)/2,0,18]) cube([wup,47,2.0]); // upper covfefe
    
    translate([10.5,-4,1.5]) rotate([0,90,0]) cylinder(d=8,h=15.5,$fn=16); //uSD
    
    difference(){ // radius
        rad= 4;
        translate([0,0,18-rad]) cube([103,rad,rad]); 
        translate([0,rad,18-rad]) rotate([0,90,0])  cylinder(r=rad,h=103);
    }
}

rotate([0,180,0])

difference(){
cover();
color("red") HC2();
}

module corner(){
    
    difference(){
        translate([1,-5.5,0])rotate([0,0,30])cube([11,11,12]);
         h=1.5;
        
        translate([0,0,h])union(){
            cube([4,6,15]);
            translate([2.2,-2,0])cube([1.8,2,15]);
            cube([2.2,16,15]);
            
            translate([7,-10,-h])cube(20);
            translate([-22,-10,-h])cube(20);
            translate([0,8,-h])cube(20);
            
        }
    }
}

translate([0,-20,0]) corner();
translate([20,-20,0]) corner();

mirror([1,0,0]) translate([20,-20,0]) corner();
mirror([1,0,0]) translate([40,-20,0]) corner();