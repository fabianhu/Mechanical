//import ("../PrusaMK3/Prusa_i3_MK3_Fusion.stl");

translate([-232.7,-10,210.5]) rotate([-90,0,-90]) import("../PrusaMK3/i3-mk2-x-strain.stl");

translate([-185,-15,198])

color("green")
camera();

camrot=[0,90+6,-11];

module camera(){

// rotate(camrot) cylinder(d=7.5,h=30); 
  //  rotate(camrot) translate([0,0,-30])cylinder(d=4,h=30); 
 //  rotate(camrot) cylinder(d=2,h=160); 
 //   rotate(camrot) translate([0,0,30]) CAMCONE(deg=60);
    
    translate([0,0,-2])
    difference(){
        union(){
            //translate([10,-3,-3]) cube([30,14,14],center=true);
            rotate(camrot) translate([0,0,-48]) rotate([0,0,45/2])cylinder(d1=13,d2=12,h=70,$fn=8);
            translate([-33,1,27.5-5]) cube([29.4,8,40],center=true);
            
            translate([14,-12,-1.8]) rotate([-90,0,0]) cylinder(d2=8,d1=6,h=6,$fn=60); // bolt2
            
        }
        rotate(camrot) translate([0,0,-50]) cylinder(d=7.6,h=100,$fn=60); // cam
        
        translate([-42.2,-40,18.2+2]) rotate([-90,0,0]) cylinder(d=8.0,h=90,$fn=60); // bolt
        
        translate([-27,-40,34]) rotate([-90,0,0]) cylinder(d=34,h=90,$fn=6); // center
        
        translate([-33,1+10+4,27.5-10]) cube([80,20,50],center=true);
        
        translate([-50,0,-5]) rotate([0,45,0]) cube([30,30,30],center=true);
        
        translate([14,-16,-1.8]) rotate([-90,0,0]) cylinder(d=2.5,h=10,$fn=60); // bolt2
        
        
    }
    
    
}

module CAMCONE(deg)
{
    dst=50;
    h0=4;
    w0=h0*4/3;//16/9;
    
    diag= (tan(deg/2)*dst)*2;
    w1=diag/5*4;
    
    scl= w1/w0;
    
    linear_extrude(height = dst, center = false, scale = scl) 
    {
        square([h0,w0],true);
    }
}

