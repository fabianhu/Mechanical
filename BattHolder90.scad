
$fn = 60;


W= 30.5-0.6; // -0.5 for strip solution
onepiece = false; // stip solution
L= 46;
H= 10.5;
/*
W= 30.5;
L= 60;
H= 30.5;
*/
r = 1.0; // battery edge radius
di = 0.85; // wall thickness

l2= min(H*0.9,(L/2)*0.8); // small hole dia
ld= min(W*0.9,(L/2)*0.9); // big hole dia

module roundedCube(W,L,H,r)
{
    minkowski()
    {
        cube([W-r*2,L-r*2,H-r*2], true);
        sphere(r=r);
    }
}

module hollowDings()
{


    
    difference()
    {
        minkowski()
        {
            roundedCube(W,L,H,r); // battery shape
            sphere(di);
        }
        
        roundedCube(W,L,H,r); // battery shape
        translate([0,L-di,0]) cube([W+di*2,L,H+di*2],true); // open end
        if(onepiece){
            // horizontal holes
                    translate([0,L/1.8,0]) cylinder(d=ld,h=H+10,center=true);
            translate([0,-L/1.8,0]) cylinder(d=ld,h=H+10,center=true);
            cylinder(d=ld*1.15,h=H+10,center=true);
            
            
            rotate([0,90,0]) union(){
                //translate([0,L/1.8,0]) cylinder(d=l2,h=W+10,center=true);
                //translate([0,-L/1.8,0]) cylinder(d=l2,h=W+10,center=true);
                translate([0,L/3.5,0]) cylinder(d=l2,h=W+10,center=true);
                translate([0,-L/3.5,0]) cylinder(d=l2,h=W+10,center=true);
                cylinder(d=l2,h=W+10,center=true);
            }
        }
        else{
            translate([0,-L/2+L/3,0]) cube([W+di*2,L,H+di*2],true); // leave only a strip (use two!)
        }
    }

}

module Cross()
{
    translate([0,4,0])
    rotate ([0,0,45])
    for(i=[0,90,180,270])
    {
        rotate([0,0,i])
        translate([0,W/2+9,0])
        union()
        {
            cube([5,12,di+1],true);
            translate([0,-2,-2.5]) rotate([45,0,0]) cube([5,8,2],true);
        }
    }
}


hollowDings();

difference()
{
     translate([0,0,H/2+di/2-0.5]) Cross();
     roundedCube(W,L,H,r); // clear out battery
    
    rotate([0,90,0]) translate([0,L/3.5,0]) cylinder(d=l2,h=W+10,center=true); // clear out hole (again)
    
    rotate([90,0,0])translate([0,0,23.5])cube([50,50,10],true);
}


