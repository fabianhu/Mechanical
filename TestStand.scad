$fn=120;

use <libCopterMotors.scad>
armlen = 100;

armwidth = 8;

module linkscrew(){
    translate([0,0,-10]) cylinder(d=2.65,h=10);
    translate([0,0,0]) cylinder(d=3.15,h=10);
    translate([0,0,2.5]) cylinder(d=5.5,h=10);

}

module screwlink(off=[10,0,0]){
    cube([50,50,0.05],center=true);
    translate([off.x/2,off.y/2,0]) linkscrew();
    translate([-off.x/2,-off.y/2,0]) linkscrew();
}

module bearing(){
    difference(){
    cylinder(d=13,h=5);
    cylinder(d=6,h=5);
    }
}    
    

module motor(ext=false){
    translate([0,5,armlen])rotate([0,90,90])motor1204(ext,3);
    if(ext){
    translate([0,5,armlen+9])rotate([90,0,0]) cylinder(d=10,h=10);
        }
}

module upper()
{   
    difference(){
        union(){
            translate([0,5,armlen])rotate([0,90,90]) translate([0,0,-5]) cylinder(d=15,h=5);
            translate([-armwidth/2,-5,0]) cube([armwidth,10,armlen]);
        }
        motor(true);
        translate([0,0,armlen-18]) rotate([120,0,0]) screwlink([0,8,0]);
        inner(true);
    }
}

module back(){
    difference(){
    union(){
        translate([-armwidth/2,-armlen,0]) cube([armwidth,armlen,10]);
        translate([0,-armlen,0]) cylinder(d=armwidth,h=10);
        }
        translate([0,-armlen,0]) cylinder(d=3.5,h=10);
        inner(true);
    }
}

bearinginnerdia=5;
bearingouterdia=16;
bearingwidth=5;
bearingwall = 5;

scaleheight = 20;


module bearingclearance(){
    difference(){
    cylinder(d=bearingouterdia+0.5,h=0.3);
    cylinder(d=(bearingouterdia+bearinginnerdia)/2,h=0.3);
    }
    
}

module inner(ext = false){
difference(){
    rotate([0,90,0])cylinder(d=bearingouterdia+bearingwall,h=armwidth,center=true);
    if(!ext){
        rotate([0,90,0]) cylinder(d=bearinginnerdia,h=armwidth,center=true);

        translate([armwidth/2,0,0]) rotate([0,-90,0])bearingclearance();
        translate([-armwidth/2,0,0]) rotate([0,90,0])bearingclearance();
    }
}
}

module lower(){
    difference(){
        basewidth = armwidth*3+1;
        baseheight = 6;
        union(){
            hull(){
                translate([0,0,-scaleheight+baseheight/2]) cube([basewidth,armwidth*2,baseheight],center=true);
                rotate([0,90,0]) cylinder(d=bearingouterdia+bearingwall,h=basewidth,center=true);
            }
            translate([0,0,-scaleheight+baseheight/2]) cube([basewidth*2,armwidth*3,baseheight],center=true);
            
            translate([-basewidth/2,5,0]) cube([basewidth,5,8]); // end stop
            
            
        }
        rotate([0,90,0])cylinder(d=bearingouterdia+bearingwall+0.8,h=armwidth+0.5,center=true); // inner arm center space
        rotate([0,90,0]) cylinder(d=bearingouterdia,h=armwidth+2*bearingwidth+0.5,center=true); // bearing clearance
        rotate([0,90,0]) cylinder(d=(bearingouterdia+bearinginnerdia)/2,h=armwidth+2*bearingwidth+12,center=true); // bearing bolt clearance
        translate([0,0,-5/2 + bearingouterdia/2]) cube([armwidth+2*bearingwidth+0.5,5,5],center=true); // top cut
        
        //translate([0,0,-50]) cube(100);
    }
}
union(){
    union(){
upper();
inner();
back();
         translate([0,-45,0]) rotate([-45,0,0]) translate([-armwidth/2,-armwidth,0]) cube([armwidth,armwidth,70]);
    }
lower();
}


//motor(false);
