/*
Copter parts ESC library

(c) 2018 Fabian Huslik

https://github.com/fabianhu/
*/ 

translate([-40,0,0]) ESC15x15();
translate([-5,0,0]) ESC20x20();

translate([-40,40,0]) ESC15x15(true);
translate([-5,40,0]) ESC20x20(true);

module ESC15x15(clr=false) 
{
    l = 15;
    PCB_x = 22.8;	
    PCB_y = 20.45;
    PCB_r = 3.5;
	hp=1.0; // PCB thickness
	
    difference()
    {
        
		color("green") hull()
        {
			for(i = [ 
			         [ (PCB_x-PCB_r)/2,   (PCB_y-PCB_r)/2,  0],
			         [ (PCB_x-PCB_r)/2, -(PCB_y-PCB_r)/2, 0],
			         [ -(PCB_x-PCB_r)/2,  (PCB_y-PCB_r)/2, 0],
			         [ -(PCB_x-PCB_r)/2,  -(PCB_y-PCB_r)/2, 0] ])
			{
				translate (i) cylinder(h=hp,r=PCB_r/2,$fn = 12);
			}
		}

		for(i = [ 
		         [  l/2,   l/2,  0],
		         [ l/2, -l/2, 0],
		         [-l/2,  l/2, 0],
		         [-l/2,  -l/2, 0] ])
		{   
			translate (i) cylinder(h=hp,d=2.15,$fn = 12);
		}
	}
	ttop=1.6;
	color("grey") translate([0,0,ttop/2+hp]) cube([19.8,10.7,ttop],true); // upper alongside block
	color("grey") translate([0,0,ttop/2+hp]) cube([6,17.8,ttop],true); // upper across block	
    tlow=0.6;
    color("grey") translate([0,0,-tlow/2]) cube([23,12,tlow],true); // lower alongside block
	color("grey") translate([0,0,-tlow/2]) cube([9.5,20,tlow],true); // lower across block


    if(clr)
    {
        color("red") translate([-11,0,-0.5]) cube([8,12,1],true); // upper connector space 
        color("red") translate([11,0,-0.5]) cube([8,12,1],true); // connector space
        for(i = [ [  l/2,   l/2,  0], [ l/2, -l/2, 0], [-l/2,  l/2, 0], [-l/2,  -l/2, 0] ])
        {   
			//color("red") translate (i) cylinder(h=1.6,d=4);
		}
        color("red") translate([0,0,0.5]) cube([30,18,4.6],true); // clear space around
    }

}

module copy_mirror(vec=[0,1,0])
{
    children();
    mirror(vec) children();
}


module ESC20x20(clr=false) 
{
	l = 20/2;
	lb= 31.0;
	wb= 27.5;
	h=1.0;
	r= 4;
	difference(){

		//PCB
        union()
        {
            color("green") hull()
            {
                w= (wb-r)/2;
                d = (wb-r)/2;
                for(i = [ 
                         [  w,   d,  0],
                         [ w, -d, 0],
                         [-w,  d, 0],
                         [-w,  -d, 0] ])
                {
                    translate (i) cylinder(h,d=r);
                }
            }
            
            color("green") translate([0,0,h/2]) cube([19.5,31,h],true);
        }
		
        for(i = [ 
		         [  l,   l,  -0.01],
		         [ l, -l, -0.01],
		         [-l,  l, -0.01],
		         [-l,  -l, -0.01] ])
		{   
			translate (i) cylinder(h+0.1,d=2.2,$fn=12);
		}
	}
	thick= clr?5:4.5;
    color("grey") translate([0,0,h/2]) cube([15,25.5,thick],true); // ok
	color("grey") translate([0,0,h/2]) cube([27,16.5,thick],true); // ok
    // color("white") translate([14,0,2.5]) cube([7,9,3],true); // connector removed
    
    // cables
    if(clr)
    {
//    copy_mirror([0,1,0]) 
//        color("red") hull(){
//        for(i = [-3.5*2.5:3.5:10] 
//		        )
//		{   
//			 translate ([ i, -8, +1.0+0.8]) rotate([90,0,0]) cylinder(h=15,d=2.5,$fn=12);
//		}
//        }
         color("red") hull(){
        copy_mirror([0,1,0]) translate ([ 11, 2.5, -1.0-0.5]) rotate([90,0,90]) cylinder(h=15,d=3.5,$fn=12);
         }
    }
    
}


