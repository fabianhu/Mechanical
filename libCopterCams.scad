/*
Copter parts Camera library

(c) 2018 Fabian Huslik

https://github.com/fabianhu/
*/ 

translate([0,0,0]) CAMERA();
translate([-20,0,0]) CAMERA800();
translate([50,0,0]) RUNCAM_NANO();
translate([70,0,0]) RUNCAM_NANO4();
translate([100,0,0]) RUNCAM_SWIFT();

translate([0,40,0]) CAMERA(true);
translate([50,40,0]) RUNCAM_NANO(true);
translate([70,40,0]) RUNCAM_NANO4(true);
translate([100,40,0]) RUNCAM_SWIFT(true);

$fn=60; 
// calculates the camera angle geometry for 4:3 format (diagonal angle)
module CAMCONE(deg)
{
    dst=10;
    h0=6;
    w0=h0*4/3;//16/9;
    
    diag= (tan(deg/2)*dst)*2;
    w1=diag/5*4;
    
    scl= w1/w0;
    
    linear_extrude(height = dst, center = false, scale = scl) 
    {
        square([w0,h0],true);
    }
}



module copy_mirror(vec=[0,1,0])
{
    children();
    mirror(vec) children();
}


// micro camera
module CAMERA(clr=false) 
{
	h_lens=6.7;
	color("grey") cylinder(d=10+0.5,h=h_lens); // lens
	
    
    color("grey") hull()
    {
        translate([0,0,-3/2]) cube([12,12,3],true); // upper block

        translate([0,0,-1.3/2-3]) cube([12.5,12.5,1.3],true); // PCB
    }


	color("grey") hull()
    {
		translate([0,0,-5]) cube([12.5,12.5,3],true);
		translate([0,0,-4]) cube([8,8,7],true);
	}
	color("grey") translate([-4.5,2.0,-7.5])  rotate([-60,0,0]) cylinder(d=4,h=2.5); // mic

	color("red") if(clr)
    {
        translate([0,0,h_lens-0.2]) CAMCONE(140); // view angle
        //hull() {
        color("grey") cylinder(d=11,h=h_lens); // lens
        //translate([0,-10,0]) color("grey") cylinder(d=10,h=h_lens); // lens ext}
		translate([0,0,-7]) cube([12.5,12.5,6],true); // conn block
		//translate([0,-10,-7]) cube([12.5,12.5,6],true); // conn block ext
        //translate([0,-10,-1.3/2-3]) cube([12.5,12.5,1.3],true); // PCB ext
        //translate([0,-10,-3/2]) cube([11.7,11.7,3],true); // upper block ext
        
	}

}

// micro camera
module CAMERA800(clr=false) 
{
	h_lens=6.5;
	color("grey") cylinder(d=10+0.5,h=h_lens); // lens
	
    
    color("grey") hull()
    {
        translate([0,0,-3/2]) cube([12.3,12.3,3],true); // upper block

        translate([0,0,-1.3/2-3]) cube([13,13,1.3],true); // PCB
    }


	color("grey") hull()
    {
		translate([0,0,-5]) cube([12.5,12.5,3],true);
		translate([0,0,-4]) cube([8,8,7],true);
	}
	color("grey") translate([-4.5,2.0,-7.5])  rotate([-60,0,0]) cylinder(d=4,h=2.5); // mic

	color("red") if(clr)
    {
        translate([0,0,h_lens-0.2]) CAMCONE(140); // view angle
        //hull() {
        color("grey") cylinder(d=11,h=h_lens); // lens
        //translate([0,-10,0]) color("grey") cylinder(d=10,h=h_lens); // lens ext}
		translate([0,0,-7]) cube([13,13,6],true); // conn block
		//translate([0,-10,-7]) cube([12.5,12.5,6],true); // conn block ext
        //translate([0,-10,-1.3/2-3]) cube([12.5,12.5,1.3],true); // PCB ext
        //translate([0,-10,-3/2]) cube([11.7,11.7,3],true); // upper block ext
        
	}

}


// micro camera
module RUNCAM_NANO(clr=false) 
{
	h_lens=11;
	
    color("grey") cylinder(d=12+0.5,h=h_lens); // lens
	color("grey") hull()
    {
        translate([0,0,-3/2]) cube([12.5,12.5,3],true); // upper block
        translate([0,0,-3-1.5]) cube([14.5,14.5,3],true); // PCB
    }

	color("grey") hull()
    {
		translate([0,0,-4]) cube([12.5,12.5,3],true);
		translate([0,0,-4]) cube([8,8,7],true);
	}

	color("red") if(clr)
    {
        translate([0,0,h_lens-0.2]) CAMCONE(140); // view angle
        //hull() {
        //color("grey") cylinder(d=11,h=h_lens); // lens
        //translate([0,-10,0]) color("grey") cylinder(d=10,h=h_lens); // lens ext}
		translate([0,0,-7-5+4]) cube([14.5,14.5,10],true); // conn block
		//translate([0,-10,-7]) cube([12.5,12.5,6],true); // conn block ext
        //translate([0,-10,-1.3/2-3]) cube([12.5,12.5,1.3],true); // PCB ext
        //translate([0,-10,-3/2]) cube([11.7,11.7,3],true); // upper block ext
        
	}

}

// micro camera
module RUNCAM_NANO4(clr=false) 
{
	h_lens=11;
	
    color("grey") cylinder(d=12+0.5,h=h_lens); // lens
	color("grey") hull()
    {
        translate([0,0,-3/2]) cube([12.5,12.5,3],true); // upper block
        translate([0,0,-3-1.5]) cube([14.5,14.5,3],true); // PCB
        rotate ([0,90,0]) cylinder(d=3.5,h=14,center=true); // drill M2
    }

	color("grey") hull()
    {
		translate([0,0,-4]) cube([12.5,12.5,3],true);
		translate([0,0,-4]) cube([8,8,7],true);
        
	}

	color("red") if(clr)
    {
        translate([0,0,h_lens-0.2]) CAMCONE(140); // view angle
        //hull() {
        //color("grey") cylinder(d=11,h=h_lens); // lens
        //translate([0,-10,0]) color("grey") cylinder(d=10,h=h_lens); // lens ext}
		translate([0,0,-7-5+4]) cube([14.5,14.5,10],true); // conn block
		//translate([0,-10,-7]) cube([12.5,12.5,6],true); // conn block ext
        //translate([0,-10,-1.3/2-3]) cube([12.5,12.5,1.3],true); // PCB ext
        //translate([0,-10,-3/2]) cube([11.7,11.7,3],true); // upper block ext
        rotate ([0,90,0]) cylinder(d=2,h=26,center=true); // drill M2
	}

}

module RUNCAM_SWIFT(ext=false) 
{
    FOV=150; // 2.1mm = 160° angle
    
    difference()
    {
        union()
        {
            color("grey") cylinder(d=12.5,h=10); // lens
            color("orange") rotate ([0,90,0]) cylinder(d=6,h=19.3,center=true); // fixing block
            color("orange") hull()
            {
                translate([7.5,-7.5,-5]) cylinder(d=4.5,h=7);
                translate([-7.5,7.5,-5]) cylinder(d=4.5,h=7);
            }
            //translate([0,0,-4.5-Conn_Len/2]) cube([19,19,9+Conn_Len],true);
            color("orange") translate([0,0,-1.5]) cube([14,13.3,7],true);
            color("darkgreen") translate([0,0,-1.9-5]) cube([19.3,19.3,3.8],true); // PCA
            color("lightgrey") translate([4.4,7.9,-2-5-3.5]) cube([10.2,3.2,3.5],true); // connector
            
            translate([0,0,-11]) 
            union()// TX
            {
                cube([19.5,19.5,8],true); 
                //translate([8,-8,-2]) cylinder(d=5.3,h=4,$fn=12);
                //translate([-8,8,-2]) cylinder(d=5.3,h=4,$fn=12);
                translate([-5,1,-1])cube([9,19,1],true); // upper PC extension
                
//                color("lightgrey")translate([-3.5,6,-2.5]) rotate([-90,0,0]) cylinder(d=1.8,h=43); // antenna
//                color("lightgrey")translate([-3.5,6,-2.5]) rotate([-90,0,0]) translate([0,0,20])cylinder(d=5,h=12); // antenna
            }
            
        }
        rotate ([0,90,0]) cylinder(d=2,h=20,center=true); // drill M2
        translate([0,-17,-20]) rotate([45,0,0]) cube([20,20,20],true); // connector
    }
    color("red")
    if(ext)
    {
        translate([0,0,10]) CAMCONE(FOV); // view angle
        rotate ([0,90,0]) cylinder(d=2.1,h=30,center=true,$fn=12); // add M2+extension in case of substraction
        //translate([0,0,-1.5]) cube([16,16,9],true); // center block
        translate([0,0,-1.5]) cube([19.3,19.3,9],true); // center block
        
        translate([-10,-6.5,-8.5]) rotate ([0,90,0]) cylinder(d=2.5,h=30,center=true); // channel pushbutton
        translate([0,0,-2-5-3.75]) cube([19.5,19.5,12],true); // TX extended for un/install
    }

}
