//color("green") translate([4,0,2.5]) import("../../../Downloads/Odroid HC2 Heatsink.stl");
$fn=60;

module side(){
    translate([3.7,0,0]) cube([2,196.05,38.7]);
    translate([1.3,0,1.5]) cube([3,196.05,23]);
    translate([0,0,23.3]) cube([3,196.05,2]); // outer brim
    translate([0,0,23.3+2]) rotate([0,45,0]) cube([3,196.05,2.5]); // outer brim avoid support
    translate([1.5,0,23.2]) cube([2.5,196.05,17]);
    translate([5.6-4.2,0,5.5]) cube([55,196.05,37.6+1]); // half infill
    translate([0,-20,44]) cube([150,250,17]);//top
}

module HC2(){
    side();
    translate([114.4,0,0]) mirror([1,0,0]) side();
}

difference(){
    union(){
        cube([12,16,48]);
        translate([0,8,0]) cylinder(d1=11,d2=28,h=48);
    }
    translate([4,0,2.5])HC2();
    translate([0,8,0]) cylinder(d=5,h=48);
    translate([0,8,0]) cylinder(d=9,h=38);
}

//color("green")translate([4,0,2.5])HC2();