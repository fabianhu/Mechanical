$fn=60;

/*
Copter parts Motors library

(c) 2018-2021 Fabian Huslik

https://github.com/fabianhu/
*/ 

translate([-100,0,0]) motor1103_3();
translate([0,0,0]) motor1103();
translate([100,-10,0]) motor1807();
translate([200,0,0]) motor1104();
translate([300,0,0]) motor1306();
translate([400,0,0]) motor1204();

translate([-100,100,0]) motor1103_3(true,3);
translate([0,100,0]) motor1103(true);
translate([100,110,0]) motor1807(true);
translate([200,100,0]) motor1104(true);
translate([300,100,0]) motor1306(true);
translate([400,100,0]) motor1204(true);



module copy_mirror(vec=[0,1,0])
{
    children();
    mirror(vec) children();
}

module motor1103_3(ext=false, de=2, dia = 3) // 3 inch prop
{
     
    union(){
    color("grey",1.0) cylinder(d1=13,d2=14.4,h=1.5);
    color("grey",1.0) translate([0,0,1.5])cylinder(d=14.4,h=1);
    if(ext)
    {
        for(i=[0:90:360])
        {
            color("red") rotate([0,0,i-45]) translate([4.5,0,-10]) cylinder(d=2,h=10);
            color("red") rotate([0,0,i-45]) translate([4.5,0,-10]) cylinder(d=4,h=10-de);
        }
        color("red") translate([10,0,1]) cube([10,4.3,2],center=true); // cable
        color("blue") translate([9,0,4]) cube([5,4,5],center=true); // room for installation of cable
    }
    color("grey") translate([0,0,-1.5])cylinder(d=4,h=1.56); // free room for bearing 
  
    
    color("grey",1.0) translate([0,0,2.5]) cylinder(d=15.0,h=5); //bell
    }
    
    color("lime",1.0) translate([0,0,3+4.5]) cylinder(d=dia*25.4+0.25,h=4.5); //prop
}


module motor1103(ext=false)
{
     
    union(){
    color("grey",1.0) cylinder(d1=13,d2=14.4,h=1.5);
    color("grey",1.0) translate([0,0,1.5])cylinder(d=14.4,h=1);
    if(ext)
    {
        for(i=[0:90:360])
        {
            color("red") rotate([0,0,i-45]) translate([4.5,0,-10]) cylinder(d=2,h=10);
            color("red") rotate([0,0,i-45]) translate([4.5,0,-10]) cylinder(d=4,h=8);
        }
        color("red") translate([10,0,1]) cube([10,4.3,2],center=true); // cable
        color("blue") translate([9,0,4]) cube([5,4,5],center=true); // room for installation of cable
    }
    color("grey") translate([0,0,-1.5])cylinder(d=4,h=1.56); // free room for bearing 
  
    
    color("grey",1.0) translate([0,0,2.5]) cylinder(d=15.0,h=5); //bell
    }
    
    color("lime",1.0) translate([0,0,3+4.5]) cylinder(d=2.0*25.4+0.25,h=4.5); //prop
}

module motor1104(ext=false)
{
    //cylinder(d=14.5,h=12.5); 
    union(){
    color("grey",1.0) cylinder(d1=13,d2=14.4,h=1.5);
    color("grey",1.0) translate([0,0,1.5])cylinder(d=14.4,h=1);
    if(ext)
    {
        for(i=[0:90:360])
        {
            color("red") rotate([0,0,i-45]) translate([4.5,0,-10]) cylinder(d=2,h=10);
            color("red") rotate([0,0,i-45]) translate([4.5,0,-10]) cylinder(d=4,h=8);
        }
        color("red") translate([10,0,1]) cube([10,4.3,2],center=true); // cable
        color("blue") translate([9,0,4]) cube([5,4,5],center=true); // room for installation of cable
    }
    color("grey") translate([0,0,-1.5])cylinder(d=4,h=1.56); // free room for bearing 
  
    
    color("grey",1.0) translate([0,0,2.5]) cylinder(d=15.0,h=5+5); //bell
    }
    if(ext)
    {
        color("red",1.0) translate([0,0,11.5]) cylinder(d=2.0*26.5+0.25,h=6.5,$fn=60); //prop
        //color("red",1.0) translate([0,0,11.5+6.5/2]) sphere(d=2.0*26.5+0.25,$fn=60);
        color("red",1.0) translate([0,0,11.5+6.5]) cylinder(d1=2.0*26.5+0.25,d2=2.0*26.5+0.25-3,h=5,$fn=60); //prop
        color("red",1.0) translate([0,0,11.5-5]) cylinder(d1=2.0*26.5+0.25-3,d2=2.0*26.5+0.25,h=5,$fn=60); //prop
    }
    else
    {
        color("lime",1.0) translate([0,0,12.5]) cylinder(d=2.0*26.5,h=4.5); //prop    
    }
}

module motor1204(ext=false, screwdist = 2, dia = 3)
{
    //cylinder(d=14.5,h=12.5); 
    union(){
    color("grey",1.0) cylinder(d1=13,d2=14.4,h=1.5);
    color("grey",1.0) translate([0,0,1.5])cylinder(d=14.4,h=1);
    if(ext)
    {
        for(i=[0:90:360])
        {
            color("red") rotate([0,0,i-45]) translate([4.5,0,-10]) cylinder(d=2,h=10);
            color("red") rotate([0,0,i-45]) translate([4.5,0,-8-screwdist]) cylinder(d=4,h=8);
        }
        color("red") translate([10,0,1]) cube([10,4.3,2],center=true); // cable
        color("blue") translate([9,0,4]) cube([5,4,5],center=true); // room for installation of cable
    }
    color("grey") translate([0,0,-1.5-1-3])cylinder(d=4,h=1.56+1+3); // free room for bearing 
  
    
    color("grey",1.0) translate([0,0,2.5]) cylinder(d=15.0,h=5+5); //bell
    }
    if(ext)
    {
        color("red",1.0) translate([0,0,11.5]) cylinder(d=dia*26.5+0.25,h=6.5,$fn=60); //prop
        //color("red",1.0) translate([0,0,11.5+6.5/2]) sphere(d=3.0*26.5+0.25,$fn=60);
        color("red",1.0) translate([0,0,11.5+6.5]) cylinder(d1=dia*26.5+0.25,d2=3.0*26.5+0.25-3,h=5,$fn=60); //prop
        color("red",1.0) translate([0,0,11.5-5]) cylinder(d1=dia*26.5+0.25-3,d2=3.0*26.5+0.25,h=5,$fn=60); //prop
    }
    else
    {
        color("lime",1.0) translate([0,0,12.5]) cylinder(d=dia*26.5,h=4.5); //prop    
    }
}

module motor1306(ext=false)
{
    //cylinder(d=14.5,h=12.5); 
    union(){
    color("grey",1.0) cylinder(d1=16,d2=16.8,h=1.5);
    color("grey",1.0) translate([0,0,1.5])cylinder(d=16.8,h=1);
    if(ext)
    {
        for(i=[0:90:360])
        {
            color("red") rotate([0,0,i-45]) translate([6,0,-10]) cylinder(d=2,h=10);
            color("red") rotate([0,0,i-45]) translate([6,0,-10]) cylinder(d=4,h=8);
        }
        color("red") translate([10,0,1]) cube([10,4.3,2],center=true); // cable
        color("blue") translate([9,0,4]) cube([5,4,5],center=true); // room for installation of cable
    }
    color("grey") translate([0,0,-3.5])cylinder(d=4,h=4.56); // free room for bearing 
  
    
    color("grey",1.0) translate([0,0,2.5]) cylinder(d=17.2,h=16.4); //bell
    }
    if(ext)
    {
        color("red",1.0) translate([0,0,11.5]) cylinder(d=3.0*26.5+0.25,h=6.5,$fn=60); //prop
        //color("red",1.0) translate([0,0,11.5+6.5/2]) sphere(d=2.0*26.5+0.25,$fn=60);
        color("red",1.0) translate([0,0,11.5+6.5]) cylinder(d1=3.0*26.5+0.25,d2=3.0*26.5+0.25-3,h=5,$fn=60); //prop
        color("red",1.0) translate([0,0,11.5-5]) cylinder(d1=3.0*26.5+0.25-3,d2=3.0*26.5+0.25,h=5,$fn=60); //prop
    }
    else
    {
        color("lime",1.0) translate([0,0,16.5]) cylinder(d=3.0*26.5,h=4.5); //prop    
    }
}

module motor1807(ext=false)
{
    propdia = 104 ; 
    union(){
    color("grey",1.0) cylinder(d1=16,d2=18.65,h=5);
    color("grey",1.0) translate([0,0,5])cylinder(d=18.65,h=18-5); // upper motor
    if(ext)
    {
        for(i=[0:90:360])
        {
            color("red") rotate([0,0,i-45]) translate([6,0,-10]) cylinder(d=2,h=10);
            color("red") rotate([0,0,i-45]) translate([6,0,-10]) cylinder(d=4,h=8);
        }
        color("red") translate([10,0,1.5]) cube([10,7,3],center=true); // cable
        color("blue") translate([9,0,4]) cube([5,5,5],center=true); // room for installation of cable
    }
    color("grey") translate([0,0,-1.5])cylinder(d=4,h=1.56); // free room for bearing 
  
    
    color("grey",1.0) translate([0,0,2.5]) cylinder(d=15.0,h=5); //bell
    }
    
    color("lime",1.0) translate([0,0,18]) cylinder(d=propdia,h=6.2,$fn=60); //prop
     if(ext)
    {
        color("red",1.0) translate([0,0,18+6.2]) cylinder(d1=propdia,d2=propdia-3,h=6.2,$fn=60); //prop
        color("red",1.0) translate([0,0,18-6.2]) cylinder(d1=propdia-3,d2=propdia,h=6.2,$fn=60); //prop
    }
    
    color("black",1.0) translate([0,0,0]) cylinder(d=9,h=31,$fn=6); //prop
   
}

